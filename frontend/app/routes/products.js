import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

  model(params) {
    return RSVP.hash({
      products: this.store.query('product', { 
        page: params.page,
        per_page: params.per_page,
        name_contains: params.name_contains,
        department_id: params.department_id,
        promotion_id: params.promotion_id
      }),
      departments: this.store.findAll('department'),
      promotions: this.store.findAll('promotion')
    });
  },

  queryParams: {
    page: {
      refreshModel: true
    },
    per_page: {
      refreshModel: true
    },
    name_contains: {
      refreshModel: true
    },
    department_id: {
      refreshModel: true
    },
    promotion_id: {
      refreshModel: true
    }
  }

});