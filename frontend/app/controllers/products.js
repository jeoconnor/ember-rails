import Controller from '@ember/controller';
import { debounce } from '@ember/runloop';
import { set } from '@ember/object';
import { computed } from '@ember/object';

export default Controller.extend({
  queryParams: ['page', 'per_page', 'name_contains', 'department_id', 'promotion_id'],
  page: 1,
  per_page: 25,
  name_contains: null,
  department_id: null,
  promotion_id: null,

  department: computed('department_id', 'model', function() {
    const departments = this.get('model.departments');
    
    if (!departments) {
      return null;
    }
    return departments.find(x => x.id == this.get('department_id'));
  }),

  promotion: computed('promotion_id', 'model', function() {
    const promotions = this.get('model.promotions');
    
    if (!promotions) {
      return null;
    }
    return promotions.find(x => x.id == this.get('promotion_id'));
  }),

  actions: {
    setPage (newPage) {
      set(this, "page", newPage);
    },
    filterNameContains (filterVal) {
      debounce(this, this.setNameContainsFilter, filterVal, 350);
    },
    filterDepartment (department) {
      set(this, "department_id", department ? department.id : null);
      this.send('setPage', 1);
    },
    filterPromotion (promotion) {
      set(this, "promotion_id", promotion ? promotion.id : null);
      this.send('setPage', 1);
    }
  },
  setNameContainsFilter: function(filterVal) {
    set(this, "name_contains", filterVal);
    this.send('setPage', 1);
  }
});