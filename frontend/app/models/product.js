import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  name: DS.attr('string'),
  price: DS.attr('number'),
  department: DS.belongsTo('department'),
  promotion: DS.belongsTo('promotion'),
  
  discounted_price: computed('price', 'promotion.{discount_amount}', function() {
    var discounted_price = parseFloat(this.get('price')) - parseFloat(this.get('promotion.discount_amount'));
    return isNaN(discounted_price) ? '--' : discounted_price.toFixed(2);
  }),
  promotion_code: computed('promotion.{promotion_code}', function() {
    return this.get('promotion.promotion_code') || '--';
  })
})