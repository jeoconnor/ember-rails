import DS from 'ember-data';

export default DS.Model.extend({
  promotion_code: DS.attr('string'),
  discount_amount: DS.attr('number')
})