class DepartmentSerializer
    include FastJsonapi::ObjectSerializer
    attributes :id, :name
    has_many :products, serializer: :product_serializer
end