class PromotionSerializer
    include FastJsonapi::ObjectSerializer
    attributes :id, :promotion_code, :discount_amount
    has_many :products, serializer: :product_serializer
end