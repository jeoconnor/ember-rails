class ProductSerializer
    include FastJsonapi::ObjectSerializer
    attributes :name, :price
    belongs_to :department, serializer: :department_serializer
    belongs_to :promotion, serializer: :promotion_serializer
end