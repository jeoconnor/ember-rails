class Product < ActiveRecord::Base
    include Filterable
    belongs_to :department
    belongs_to :promotion, optional: true
    scope :department_id, -> (department_id) { where department_id: department_id }
    scope :promotion_id, -> (promotion_id) { where promotion_id: promotion_id }
    scope :name_contains, -> (name) { where("name ILIKE ?", "%#{name}%")}
end
