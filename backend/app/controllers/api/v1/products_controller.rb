module Api
  module V1
    class ProductsController < ApplicationController
      def index
        products = Product.filter(params.slice(:department_id, :promotion_id, :name_contains)).page(params[:page]).per(params[:per_page])
        options = pagination_options(products)

        render json: ProductSerializer.new(products, options).serialized_json
      end

      def show
        product = Product.find(params[:id])
        render json: ProductSerializer.new(product).serialized_json
      end
    end
  end
end