module Api
  module V1
    class DepartmentsController < ApplicationController
      def index
        departments = Department.page(params[:page]).per(params[:per_page])
        options = pagination_options(departments)

        render json: DepartmentSerializer.new(departments, options).serialized_json
      end

      def show
        department = Department.find(params[:id])
        render json: DepartmentSerializer.new(department).serialized_json
      end
    end
  end
end