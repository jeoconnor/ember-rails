module Api
  module V1
    class PromotionsController < ApplicationController
      def index
        promotions = Promotion.page(params[:page]).per(params[:per_page])
        options = pagination_options(promotions)
        
        render json: PromotionSerializer.new(promotions, options).serialized_json
      end

      def show
        promotion = Promotion.find(params[:id])
        render json: PromotionSerializer.new(promotion).serialized_json
      end
    end
  end
end