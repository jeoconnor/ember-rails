class ApplicationController < ActionController::API
  def pagination_options(paginated_data)
    current_page = params[:page].to_i
    per_page = params[:per_page].to_i
    total_count = paginated_data.total_count

    view_start_amount = (current_page-1) * per_page
    view_end_amount = view_start_amount + per_page
    if view_end_amount > total_count
      view_end_amount = total_count
    end

    options = {}
    options[:meta] = { 
      current_page: current_page,
      last_page: paginated_data.total_pages,
      view_start_amount: view_start_amount,
      view_end_amount: view_end_amount,
      count: total_count
    }
    return options
  end
end
