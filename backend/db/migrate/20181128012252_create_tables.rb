class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :promotions do |t|
      t.text :promotion_code, null: false
      t.decimal :discount_amount, precision: 8, scale: 2, null: false

      t.timestamps
    end

    create_table :departments do |t|
      t.text :name

      t.timestamps
    end

    create_table :products do |t|
      t.text :name, null: false
      t.decimal :price, precision: 8, scale: 2, null: false
      t.references :department, null: false, foreign_key: true, index: true
      t.references :promotion, null: true, foreign_key: true, index: true

      t.timestamps
    end
  end
end
