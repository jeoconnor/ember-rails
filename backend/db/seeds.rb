# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
    require 'faker'
    require 'populator'
    require 'populator_fixes.rb'

    [Product, Promotion, Department].each(&:delete_all)
    ActiveRecord::Base.connection.tables.each do |t|
        ActiveRecord::Base.connection.reset_pk_sequence!(t)
    end

    Promotion.populate 10..20 do |promotion|
        promotion.discount_amount = Faker::Commerce.price(range = 0..75.0, as_string = false)
        promotion.promotion_code = Faker::Commerce.unique.promotion_code(0)
    end

    Department.populate 20 do |department|
        department.name = Faker::Commerce.unique.department(1, true)

        Product.populate 10..20 do |product|
            product.name = Faker::Commerce.product_name
            product.price = Faker::Commerce.price(range = 0..350.0, as_string = false)
            product.department_id = department.id
            if(product.price > rand(350))
                product.promotion_id = Promotion.find(Promotion.pluck(:id).sample)
            end
        end
    end
end