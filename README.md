### Setup

Requires [Docker Compose](https://docs.docker.com/compose/install/)

Navigate to the base folder of the project and run build.

```sh
$ docker-compose build
```
Then wait a few minutes while it downloads all the dependencies required to run. Once that is finished then set up the database using rake in the backend service.
```sh
$ docker-compose run --rm backend rake db:reset
```

Once the database is setup and populated from the ```rake db:reset``` command then you can start running everything.
```sh
$ docker-compose up
```

Once the services have started up you can view the frontend ember app by going to ```http://localhost:4200``` in your browser. To get to the backend rails API go to ```http://localhost:9292```.

### Deploy

There is an AWS EC2 instance that has a [GitLab Runner service](https://docs.gitlab.com/runner/) running and connected this repository. It uses [Gitlab's CI/CD pipeline](https://gitlab.com/jeoconnor/ember-rails/pipelines) to trigger builds when code changes are made on the ```master``` branch.

The runner will pick up the changes and build the images on the EC2 server. The new build is set up so that it needs to be manually deployed. The steps the runner takes is outlined in the [.gitlab-ci.yml](https://gitlab.com/jeoconnor/ember-rails/blob/master/.gitlab-ci.yml) file in the base of the project.

Once the build has been deployed by the runner then you can go to http://35.183.101.36:4200/ to view the changes on the server.